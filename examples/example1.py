"""

    Example of the cinferno colormap.

"""

import numpy as np
from matplotlib import pyplot as plt
import cmocean

from cinferno import cmap


z = np.loadtxt('example_data')
x = np.linspace(0, 60.e3, 120)
y = np.linspace(0, 60.e3, 120)
X, Y = np.meshgrid(x, y)

fig, ((ax1, ax2), (ax3, ax4), (ax5, ax6)) = plt.subplots(nrows=3, ncols=2, figsize=(10, 10), sharex=True,sharey=True)
mp1 = ax1.pcolormesh(X*1.e-3, Y*1.e-3, z, cmap=cmap.cinferno, vmin=0.99, vmax=1.01)
mp2 = ax2.pcolormesh(X*1.e-3, Y*1.e-3, z, cmap='inferno', vmin=0.99, vmax=1.01)
mp3 = ax3.pcolormesh(X*1.e-3, Y*1.e-3, z, cmap='cividis', vmin=0.99, vmax=1.01)
mp4 = ax4.pcolormesh(X*1.e-3, Y*1.e-3, z, cmap='Spectral_r', vmin=0.99, vmax=1.01)
mp5 = ax5.pcolormesh(X*1.e-3, Y*1.e-3, z, cmap='seismic', vmin=0.99, vmax=1.01)
mp6 = ax6.pcolormesh(X*1.e-3, Y*1.e-3, z, cmap='RdYlGn_r', vmin=0.99, vmax=1.01)

fig.colorbar(mp1, ax=ax1)
fig.colorbar(mp2, ax=ax2)
fig.colorbar(mp3, ax=ax3)
fig.colorbar(mp4, ax=ax4)
fig.colorbar(mp5, ax=ax5)
fig.colorbar(mp6, ax=ax6)

fig.suptitle('Colormap comparison', fontsize=14)

ax1.set_title('cinferno')
ax2.set_title('inferno')
ax3.set_title('cividis')
ax4.set_title('Spectral_r')
ax5.set_title('seismic')
ax6.set_title('RdYlGn_r')

fig.subplots_adjust(wspace=0.1, top=0.925, bottom=0.05, left=0.05, right=0.925)

fig.savefig('cinferno.png', dpi=300)
plt.close(fig)
