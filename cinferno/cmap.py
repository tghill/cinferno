"""

    cinferno.py

    Module to create matplotlib colormap cinferno.

    This is a colormap based on the matplotlib colormap inferno,
    after processing it through the scripts provided by this paper
    http://journals.plos.org/plosone/article/comments?id=10.1371/journal.pone.0199239

"""

import os
import numpy as np
from matplotlib import colors

print(os.path.abspath(__file__))

data = os.path.join(os.path.split(os.path.abspath(__file__))[0], 'cinferno.txt')
cinferno = colors.ListedColormap(np.loadtxt(data))
